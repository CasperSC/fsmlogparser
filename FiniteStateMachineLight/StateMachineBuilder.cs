﻿using System;
using System.Collections.Generic;
using FiniteStateMachineLight.Interfaces;

namespace FiniteStateMachineLight
{
    public class StateMachineBuilder<TState, TEvent, TContext>
        where TEvent : struct
        where TState : struct
    {
        private readonly List<Transition<TState, TEvent, TContext>> _transitions;
        private TState _initialState;
        private IState<TContext, TEvent, TState> _initialStateImplementation;

        public StateMachineBuilder()
        {
            _transitions = new List<Transition<TState, TEvent, TContext>>();
        }

        public StateMachineBuilder<TState, TEvent, TContext> AddTransition<TStateImplementation>(TState currentState, TEvent @event) 
            where TStateImplementation : IState<TContext, TEvent, TState>
        {
            var impl = Activator.CreateInstance<TStateImplementation>();
            _transitions.Add(new Transition<TState, TEvent, TContext>(
                new TransitionRule<TState, TEvent>(currentState, @event), impl.State, impl));

            return this;
        }

        public StateMachineBuilder<TState, TEvent, TContext> SetInitialState<TStateImplementation>(TState state)
            where TStateImplementation : IState<TContext, TEvent, TState>
        {
            _initialState = state;
            _initialStateImplementation = Activator.CreateInstance<TStateImplementation>();

            return this;
        }

        public StateMachine<TState, TEvent, TContext> Build()
        {
            if (_initialStateImplementation == null)
            {
                throw new InvalidOperationException($"{_initialStateImplementation}  == null");
            }

            return new StateMachine<TState, TEvent, TContext>(_initialState, _initialStateImplementation, _transitions);
        }
    }
}