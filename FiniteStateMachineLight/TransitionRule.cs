﻿using System;
using System.Collections.Generic;

namespace FiniteStateMachineLight
{
    /// <summary>
    /// Правило для перехода состояния. 
    /// </summary>
    /// <typeparam name="TState"> Состояние, необходимое для перехода. </typeparam>
    /// <typeparam name="TEvent"> Событие, необходимое для перехода. </typeparam>
    public class TransitionRule<TState, TEvent> : IEquatable<TransitionRule<TState, TEvent>>
    {
        public TEvent Event { get; }

        public TState State { get; }

        public TransitionRule(TState state, TEvent @event)
        {
            State = state;
            Event = @event;
        }

        public bool Equals(TransitionRule<TState, TEvent> other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return EqualityComparer<TState>.Default.Equals(State, other.State) && EqualityComparer<TEvent>.Default.Equals(Event, other.Event);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TransitionRule<TState, TEvent>)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (EqualityComparer<TState>.Default.GetHashCode(State) * 397) ^ EqualityComparer<TEvent>.Default.GetHashCode(Event);
            }
        }
    }
}