﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FiniteStateMachineLight.Interfaces;

namespace FiniteStateMachineLight
{
    public class StateMachine<TState, TEvent, TContext> : IStateMachine<TState, TContext, TEvent>
    {
        private readonly Dictionary<
            TransitionRule<TState, TEvent>,
            Transition<TState, TEvent, TContext>> _transitions;

        private TState _currentState;
        private IState<TContext, TEvent, TState> _currentStateImpl;

        public TState CurrentState
        {
            get { return _currentState; }
        }

        public StateMachine(TState initialState, IState<TContext, TEvent, TState> initialStateImpl, List<Transition<TState, TEvent, TContext>> transitions)
        {
            _transitions = new Dictionary<
                TransitionRule<TState, TEvent>,
                Transition<TState, TEvent, TContext>>();

            transitions.ForEach((transition => _transitions.Add(transition.Rule, transition)));

            _currentState = initialState;
            _currentStateImpl = initialStateImpl;
        }

        public void SendEvent(TEvent command)
        {
            var transition = _transitions[new TransitionRule<TState, TEvent>(_currentState, command)];
            _currentStateImpl = transition.NextStateImplementation;
            _currentState = transition.NextState;
        }

        public void Stop()
        {
            _currentStateImpl = null;
        }

        public async Task Start(TContext context)
        {
            while (_currentStateImpl != null)
            {
                await _currentStateImpl.DoWork(this, context);
            }
        }
    }
}