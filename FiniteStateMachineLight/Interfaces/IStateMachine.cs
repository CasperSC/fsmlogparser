﻿namespace FiniteStateMachineLight.Interfaces
{
    public interface IStateMachine<TState, TContext, TEvent>
    {
        TState CurrentState { get; }

        void Stop();

        void SendEvent(TEvent command);
    }
}