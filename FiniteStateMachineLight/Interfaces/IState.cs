﻿using System.Threading.Tasks;

namespace FiniteStateMachineLight.Interfaces
{
    public interface IState<TContext, TEvent, TState>
    {
        TState State { get; }

        Task DoWork(IStateMachine<TState, TContext, TEvent> fsm, TContext context);
    }
}