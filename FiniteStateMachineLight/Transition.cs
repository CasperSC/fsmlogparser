﻿using FiniteStateMachineLight.Interfaces;

namespace FiniteStateMachineLight
{
    public class Transition<TState, TEvent, TContext>
    {
        public TransitionRule<TState, TEvent> Rule { get; }

        public TState NextState { get; }

        public IState<TContext, TEvent, TState> NextStateImplementation { get; }

        public Transition(TransitionRule<TState, TEvent> rule, TState nextState, IState<TContext, TEvent, TState> nextStateImplementation)
        {
            Rule = rule;
            NextState = nextState;
            NextStateImplementation = nextStateImplementation;
        }
    }
}