﻿using System;
using System.Threading.Tasks;
using FiniteStateMachineLight.Interfaces;
using FsmLogParser.States.Enums;

namespace FsmLogParser.States
{
    /// <summary>
    /// <see cref="ParserState.Done"/>.
    /// </summary>
    public class DoneState : IState<Program, ParserEvent, ParserState>
    {
        public ParserState State { get; } = ParserState.Done;

        public DoneState()
        {
        }

        public Task DoWork(IStateMachine<ParserState, Program, ParserEvent> fsm, Program context)
        {
            Console.WriteLine($"Current state: {fsm.CurrentState}. The end.");
            fsm.Stop();
            return Task.CompletedTask;
        }
    }
}