﻿namespace FsmLogParser.States.Enums
{
    public enum ParserEvent
    {
        /// <summary> Блок начала сессии найден. </summary>
        StartSessionFound,

        /// <summary> Отправитель найден. </summary>
        SenderFound,

        /// <summary> Получатель найден. </summary>
        RecipientFound,

        /// <summary> Конец блока найден. </summary>
        EndOfBlockFound,

        /// <summary> Маркер успешного завершения сессии найден. </summary>
        SuccessfulSessionTokenFound,

        /// <summary> Маркер завершения сессии найден. </summary>
        EndOfSessionFound,

        /// <summary> Конец диапазона поиска достигнут (например, конец файла или ограниченного диапазона). </summary>
        EndOfSearchRangeReached
    }
}