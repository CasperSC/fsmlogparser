﻿namespace FsmLogParser.States.Enums
{
    public enum ParserState
    {
        /// <summary> Поиск блока начала сессии. </summary>
        StartSessionSearch,

        /// <summary> Поиск отправителя. </summary>
        SenderSearch,

        /// <summary> Поиск получателя. </summary>
        RecipientSearch,

        /// <summary> Поиск маркера успешного завершения сессии. </summary>
        SessionTokenSearch,

        /// <summary> Поиск завершён. </summary>
        Done
    }
}