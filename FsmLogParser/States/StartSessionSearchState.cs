﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FiniteStateMachineLight.Interfaces;
using FsmLogParser.States.Enums;

namespace FsmLogParser.States
{
    /// <summary>
    /// <see cref="ParserState.StartSessionSearch"/>.
    /// </summary>
    public class StartSessionSearchState : IState<Program, ParserEvent, ParserState>
    {
        private readonly Regex _regex;

        public ParserState State { get; } = ParserState.StartSessionSearch;

        public StartSessionSearchState()
        {
            var options = RegexOptions.Compiled | RegexOptions.IgnoreCase;
            _regex = new Regex(@"Accepting SMTP connection from \d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}:\d{1,10} to \d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}:\d{1,10}", options);
        }

        public async Task DoWork(IStateMachine<ParserState, Program, ParserEvent> fsm, Program context)
        {
            context.IterationCounter++;

            while (true)
            {
                string line = await context.GetNextLine();

                if (line == null)
                {
                    Console.WriteLine($"Current state: {fsm.CurrentState}. SendEvent({nameof(ParserEvent.EndOfSearchRangeReached)})");
                    fsm.SendEvent(ParserEvent.EndOfSearchRangeReached);
                    break;
                }

                Match match = await Task.Run(() => _regex.Match(line));
                if (match.Success)
                {
                    Console.WriteLine($"Current state: {fsm.CurrentState}. SendEvent({nameof(ParserEvent.StartSessionFound)})");
                    fsm.SendEvent(ParserEvent.StartSessionFound);
                    break;
                }
            }
        }
    }
}