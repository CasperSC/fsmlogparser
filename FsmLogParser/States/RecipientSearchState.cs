﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FiniteStateMachineLight.Interfaces;
using FsmLogParser.States.Enums;

namespace FsmLogParser.States
{
    /// <summary>
    /// <see cref="ParserState.RecipientSearch"/>.
    /// </summary>
    public class RecipientSearchState : IState<Program, ParserEvent, ParserState>
    {
        private readonly Regex _regex;

        public ParserState State { get; } = ParserState.RecipientSearch;

        public RecipientSearchState()
        {
            var options = RegexOptions.Compiled | RegexOptions.IgnoreCase;
            _regex = new Regex(@"RCPT TO:\s+<{0,1}([a-zA-Z0-9-]+@[a-zA-Z0-9-]+\.[a-zA-Z]+)>{0,1}", options);
        }

        public async Task DoWork(IStateMachine<ParserState, Program, ParserEvent> fsm, Program context)
        {
            while (true)
            {
                string line = await context.GetNextLine();

                if (line == null)
                {
                    Console.WriteLine($"Current state: {fsm.CurrentState}. SendEvent({nameof(ParserEvent.EndOfSearchRangeReached)})");
                    fsm.SendEvent(ParserEvent.EndOfSearchRangeReached);
                    break;
                }

                Match match = await Task.Run(() => _regex.Match(line));
                if (match.Success)
                {
                    Console.WriteLine($"Current state: {fsm.CurrentState}. SendEvent({nameof(ParserEvent.RecipientFound)})");
                    if (match.Groups.Count > 1)
                    {
                        var info = context.GetSessionInfo(context.IterationCounter);
                        info.Recipients.Add(match.Groups[1].Value);
                    }
                    fsm.SendEvent(ParserEvent.RecipientFound);
                    break;
                }

                if (await Task.Run(() => Regex.IsMatch(line, @"\s+QUIT$", RegexOptions.Compiled)))
                {
                    Console.WriteLine($"Current state: {fsm.CurrentState}. SendEvent({nameof(ParserEvent.EndOfBlockFound)})");
                    fsm.SendEvent(ParserEvent.EndOfBlockFound);
                    break;
                }
            }
        }
    }
}