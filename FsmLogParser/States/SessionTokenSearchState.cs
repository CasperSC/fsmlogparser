﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FiniteStateMachineLight.Interfaces;
using FsmLogParser.States.Enums;

namespace FsmLogParser.States
{
    /// <summary>
    /// <see cref="ParserState.SessionTokenSearch"/>.
    /// </summary>
    public class SessionTokenSearchState : IState<Program, ParserEvent, ParserState>
    {
        public ParserState State { get; } = ParserState.SessionTokenSearch;

        public SessionTokenSearchState()
        {
        }

        public async Task DoWork(IStateMachine<ParserState, Program, ParserEvent> fsm, Program context)
        {
            while (true)
            {
                string line = await context.GetNextLine();

                if (line == null)
                {
                    Console.WriteLine($"Current state: {fsm.CurrentState}. SendEvent({nameof(ParserEvent.EndOfSearchRangeReached)})");
                    fsm.SendEvent(ParserEvent.EndOfSearchRangeReached);
                    break;
                }

                if (await Task.Run(() => Regex.IsMatch(line, "SMTP session successful")))
                {
                    Console.WriteLine($"Current state: {fsm.CurrentState}. SendEvent({nameof(ParserEvent.SuccessfulSessionTokenFound)})");
                    var info = context.GetSessionInfo(context.IterationCounter);
                    info.Successful = true;

                    fsm.SendEvent(ParserEvent.SuccessfulSessionTokenFound);
                    break;
                }
                if (await Task.Run(() => Regex.IsMatch(line, @"\s+----------$", RegexOptions.Compiled)))
                {
                    Console.WriteLine($"Current state: {fsm.CurrentState}. SendEvent({nameof(ParserEvent.EndOfSessionFound)})");
                    fsm.SendEvent(ParserEvent.EndOfSessionFound);
                    break;
                }
            }
        }
    }
}