﻿using System.Collections.Generic;
using System.Linq;

namespace FsmLogParser.Model
{
    public class SessionInfo
    {
        public string Sender { get; set; }

        public List<string> Recipients { get; }

        public bool Successful { get; set; }

        public SessionInfo()
        {
            Recipients = new List<string>();
        }

        public override string ToString()
        {
            string recipients = Recipients.Any()
                ? Recipients.Aggregate((workingSentence, next) => next + ", " + workingSentence)
                : string.Empty;
            
            return (Sender ?? "") + 
                   (recipients != string.Empty ? ", " : "") + 
                   recipients +
                   (Successful ? " SMTP session successful" : "");
        }
    }
}