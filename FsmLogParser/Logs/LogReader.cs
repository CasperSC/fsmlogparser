﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace FsmLogParser.Logs
{
    public class LogReader : IDisposable
    {
        private FileStream _fileStream;
        private BufferedStream _bufferedStream;
        private StreamReader _reader;

        public string PathToFile { get; }

        public LogReader(string pathToFile)
        {
            PathToFile = pathToFile;
        }

        public void OpenFile()
        {
            _fileStream = File.Open(PathToFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            _bufferedStream = new BufferedStream(_fileStream);
            _reader = new StreamReader(_bufferedStream);
        }

        public Task<string> ReadLine()
        {
            return _reader.ReadLineAsync();
        }

        public void Dispose()
        {
            _reader?.Dispose();
            _bufferedStream?.Dispose();
            _fileStream?.Dispose();
        }
    }
}