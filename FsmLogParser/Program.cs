﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using FiniteStateMachineLight;
using FsmLogParser.Logs;
using FsmLogParser.Model;
using FsmLogParser.States;
using FsmLogParser.States.Enums;

namespace FsmLogParser
{
    public class Program
    {
        #region Entry point

        private static Program _program;

        private static Task<int> Main(string[] args)
        {
            _program = new Program();
            return _program.Run(args);
        }

        #endregion

        private const int NOT_ENOUGH_POSITIONAL_CMD_ARGS_SPECIFIED_ERROR = 1;
        private const int FILE_NOT_FOUND_ERROR = 2;
        private const int EXIT_SUCCESS = 0;

        private LogReader _logReader;
        private Dictionary<int, SessionInfo> _infos;

        public int IterationCounter { get; set; }

        private async Task<int> Run(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Not enough positional command-line arguments specified!");
                return NOT_ENOUGH_POSITIONAL_CMD_ARGS_SPECIFIED_ERROR;
            }

            string pathToFile = args[0];
            if (!File.Exists(pathToFile))
            {
                Console.WriteLine($"File \"{pathToFile}\" not found.");
                return FILE_NOT_FOUND_ERROR;
            }

            try
            {
                var fsm = BuildStateMachine();

                _infos = new Dictionary<int, SessionInfo>();
                using (_logReader = new LogReader(pathToFile))
                {
                    _logReader.OpenFile();
                    await fsm.Start(this);
                }

                Console.WriteLine("Results:");
                foreach (var (iteration, sessionInfo) in _infos)
                {
                    Console.WriteLine($"{iteration.ToString()}: {sessionInfo}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return EXIT_SUCCESS;
        }

        private static StateMachine<ParserState, ParserEvent, Program> BuildStateMachine()
        {
            var builder = new StateMachineBuilder<ParserState, ParserEvent, Program>();

            // Схема машины состояний: https://i.imgur.com/KSy6DLB.jpg
            builder.SetInitialState<StartSessionSearchState>(ParserState.StartSessionSearch)
                // Перейти в состояние SenderSearchState, если сейчас состояние
                // ParserState.StartSessionSearch и пришло событие ParserEvent.StartSessionFound
                .AddTransition<SenderSearchState>(ParserState.StartSessionSearch, ParserEvent.StartSessionFound)
                .AddTransition<RecipientSearchState>(ParserState.SenderSearch, ParserEvent.SenderFound)
                .AddTransition<RecipientSearchState>(ParserState.RecipientSearch, ParserEvent.RecipientFound)
                .AddTransition<SessionTokenSearchState>(ParserState.RecipientSearch, ParserEvent.EndOfBlockFound)
                .AddTransition<StartSessionSearchState>(ParserState.SessionTokenSearch, ParserEvent.SuccessfulSessionTokenFound)
                .AddTransition<StartSessionSearchState>(ParserState.SessionTokenSearch, ParserEvent.EndOfSessionFound)
                .AddTransition<DoneState>(ParserState.StartSessionSearch, ParserEvent.EndOfSearchRangeReached)
                .AddTransition<DoneState>(ParserState.SenderSearch, ParserEvent.EndOfSearchRangeReached)
                .AddTransition<DoneState>(ParserState.RecipientSearch, ParserEvent.EndOfSearchRangeReached)
                .AddTransition<DoneState>(ParserState.SessionTokenSearch, ParserEvent.EndOfSearchRangeReached);

            return builder.Build();
        }

        public Task<string> GetNextLine()
        {
            return _logReader.ReadLine();
        }

        public SessionInfo GetSessionInfo(int iteration)
        {
            if (!_infos.ContainsKey(iteration))
            {
                _infos.Add(iteration, new SessionInfo());
            }

            return _infos[iteration];
        }
    }
}
